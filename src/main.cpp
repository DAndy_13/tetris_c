//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stdlib.h>

#include "TTetris.h"
#include "TScoreList.h"
#include "main.h"
#include "F_About_Unit.h"
#include "F_Scores_Unit.h"
#include "F_Player_Unit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "sSkinManager"
#pragma link "sSkinProvider"
#pragma resource "*.dfm"




TF_Main *F_Main;

HHOOK hook;
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);


TmyShape *Line      = new TmyLine(clRed);
TmyShape *Piramide  = new TmyPiramide(clLime);
TmyShape *RightL    = new TmyRightL(clBlue);
TmyShape *LeftL     = new TmyLeftL(clGreen);
TmyShape *LeftZ     = new TmyLeftZ(clFuchsia);
TmyShape *RightZ    = new TmyRightZ(clAqua);
TmyShape *Sqare     = new TmySqare(clPurple);



//TmyShape *pShape = 0, *pNextShape = 0;//
TTetris     *Tetris;
TScoreList  *ScoreList;

bool        mGameOver = false;
AnsiString  PlayerName = "Player";



/********************************************************************
********** ������� ���������
*********************************************************************/
#define NUMHOOKS 7
#define KEYBOARD 0


typedef struct _MYHOOKDATA
{
	int nType;
	HOOKPROC hkprc;
	HHOOK hhook;
} MYHOOKDATA;

MYHOOKDATA myhookdata[NUMHOOKS];
//void __fastcall TF_Main::KeyboardProc(TMessage &Message)
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
//    static int i = 0;

	if (nCode < 0)  // ��������� �� ������������
		return CallNextHookEx(myhookdata[KEYBOARD].hhook,
			   nCode, wParam, lParam);
    // ���� ������� ������...
    if(!(lParam>>31)&1){
        Tetris->KeyPressProc(wParam, 0);
    }
    else{
        Tetris->KeyPressProc(wParam, 1);
    }
    return CallNextHookEx(myhookdata[KEYBOARD].hhook, nCode, wParam, lParam);    
}





//---------------------------------------------------------------------------
__fastcall TF_Main::TF_Main(TComponent* Owner)
    : TForm(Owner)
{
    
	myhookdata[KEYBOARD].nType = WH_KEYBOARD;
	myhookdata[KEYBOARD].hkprc = (HOOKPROC)KeyboardProc;

    myhookdata[KEYBOARD].hhook = SetWindowsHookEx(
							myhookdata[KEYBOARD].nType,
							myhookdata[KEYBOARD].hkprc,
							(HINSTANCE) NULL, GetCurrentThreadId());


    F_Main->DoubleBuffered = true;

    Tetris = new TTetris(40, 15, MainImg, NextShapeImg);
    LevelLbl->Left  = MainImg->Left + MainImg->Width + _STEP;
    ScoreLbl->Left  = MainImg->Left + MainImg->Width + _STEP;

    F_Main->Width   = MainImg->Width + NextShapeImg->Width + _STEP*6;
    F_Main->Height  = MainImg->Height + _STEP*6;


    AnsiString Path = ExtractFilePath(ParamStr(0));
    AnsiString FileName = Path +"BestScores.dat";
    ScoreList = new TScoreList( 10, FileName );

    GameTimer->Enabled = true;
}
//---------------------------------------------------------------------------



void __fastcall TF_Main::FormCreate(TObject *Sender)
{
    F_Main->Position    = poDesktopCenter;
    F_Main->Caption     = F_Main->Caption + AnsiString(RELEASE_ID);

}
//---------------------------------------------------------------------------



void __fastcall TF_Main::GameTimerTimer(TObject *Sender)
{
    Tetris->Game();

    // �������� ���������
    if(Tetris->IsGameOver() && !mGameOver){
        ScoreList->Add(Tetris->GetScore(), PlayerName);
        mGameOver = true;
    }

    LevelLbl->Caption = "Level = "+AnsiString(Tetris->GetLevel());
    ScoreLbl->Caption = "Score = "+AnsiString(Tetris->GetScore());
}
//---------------------------------------------------------------------------



void __fastcall TF_Main::mNewGameClick(TObject *Sender)
{
    Tetris->NewGame();
    mGameOver = false;
}
//---------------------------------------------------------------------------

void __fastcall TF_Main::mPauseClick(TObject *Sender)
{
    Tetris->Pause();
}
//---------------------------------------------------------------------------




void __fastcall TF_Main::mBestScoresClick(TObject *Sender)
{
//    Tetris->GetScoreList(ScoresLV);
    F_Scores->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TF_Main::Button1Click(TObject *Sender)
{
/*
    static int iii = 125;
//    ScoreList->Add(iii++, "aaa");
    ScoreList->Add(StrToInt(Edit1->Text), Edit2->Text);
*/    
}
//---------------------------------------------------------------------------


void __fastcall TF_Main::About1Click(TObject *Sender)
{
    F_About->ShowModal();
}
//---------------------------------------------------------------------------



void __fastcall TF_Main::Button2Click(TObject *Sender)
{
//    ScoreList->Add(125, PlayerName);
/*
    myList_type myListItem;//ScoreList->GetItem(0);
    int iii = 0;//myListItem->Score;
    for(int i = 0; i < ScoreList->GetCount(); i++)
    {
        ScoreList->GetItem(i, &myListItem);
        iii = myListItem.Score;
    }
*/       
}
//---------------------------------------------------------------------------


void __fastcall TF_Main::Button3Click(TObject *Sender)
{
//    PlayerGroupBox->Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall TF_Main::mPlayerClick(TObject *Sender)
{
//    PlayerGroupBox->Visible = true;
    F_Player->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TF_Main::FormShow(TObject *Sender)
{
    F_Player->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TF_Main::mGridOnClick(TObject *Sender)
{
    TMenuItem *pItem = dynamic_cast<TMenuItem *>(Sender);

    if(!pItem)
        return;

    //pLineSeries->Pointer->Visible = pItem->Tag;
    Tetris->BoolGrid(pItem->Tag);
    pItem->Checked = true;
}
//---------------------------------------------------------------------------


