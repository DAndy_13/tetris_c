//---------------------------------------------------------------------------

#ifndef TShapeH
#define TShapeH

#include <vcl.h>
/*
enum ShapeType {
    line = 0,
    square,
    rightL,
    leftL,
    pyramide,
    leftZ,
    rightZ
};
*/

typedef enum{
    Left = 1,
    Right,
    Down,
    Up
}ShapeDir_type;

class TmyShape
{
    protected:
            int     size;
            TColor  color;

    private:
        	void	Clear(); // clear the 4x4 matrix
            TPoint  pos;

    public:
        	int	    **cells;//[size][size]; // 4x4 matrix that conatains the shape

            TmyShape(TColor  _color);
            ~TmyShape();

            TPoint  GetPos(){ return pos;};
            void    SetPos(TPoint _pos){pos = _pos;};

            int     GetSize(){return size;};
            TColor  GetColor(){return color;};
            void    Move(ShapeDir_type dir);
            
        	virtual void   Rotate();
        	virtual void   UnRotate();
            virtual AnsiString Print() = 0; // =0 - ����� �����������
//          AnsiString Print(){return "������� ����� TmyShape";}
};




class TmySqare: public TmyShape
{
    public:
    TmySqare(TColor  _color);
    ~TmySqare(){};
//    AnsiString Print(){return "�������� ����� TmySqare";}
    #pragma option push -w-inl
    AnsiString Print(){return "TmySqare";}
    #pragma option pop

};





class TmyLine: public TmyShape
{
    public:
    TmyLine(TColor  _color);
    ~TmyLine(){};
    #pragma option push -w-inl
    AnsiString Print(){return "TmyLine";}
    #pragma option pop
};



class TmyRightL: public TmyShape
{
    public:
    TmyRightL(TColor  _color);
    ~TmyRightL(){};
    #pragma option push -w-inl
    AnsiString Print(){return "TmyRightL";}
    #pragma option pop

};


class TmyLeftL: public TmyShape
{
    public:
    TmyLeftL(TColor  _color);
    ~TmyLeftL(){};
    #pragma option push -w-inl
    AnsiString Print(){return "TmyLeftL";}
    #pragma option pop  
};


class TmyLeftZ: public TmyShape
{
    public:
    TmyLeftZ(TColor  _color);
    ~TmyLeftZ(){};
    #pragma option push -w-inl
    AnsiString Print(){return "TmyLeftZ";}
    #pragma option pop
};


class TmyRightZ: public TmyShape
{
    public:
    TmyRightZ(TColor  _color);
    ~TmyRightZ(){};
    #pragma option push -w-inl 
    AnsiString Print(){return "TmyRightZ";}
    #pragma option pop

};

class TmyPiramide: public TmyShape
{
    public:
    TmyPiramide(TColor  _color);
    ~TmyPiramide(){};
    #pragma option push -w-inl
    AnsiString Print(){return "TmyPiramide";}
    #pragma option pop         
};
//---------------------------------------------------------------------------
#endif
