//---------------------------------------------------------------------------

#ifndef TTetrisH
#define TTetrisH

#include <vcl.h>
#include "TShape.h"


#define _STEP       15

#define FALL_SPEED  0
#define MIN_SPEED   21
#define MAX_LEVEL   20

#define MAX_LINES   30


#define _BackColor   clBlack//clGray

class TTetris
{
    protected:

    private:
            int     RowCount, ColCount;
            TColor  **Field;
            int     Step;

            TPoint  StartPos;

            int     Level;
            int     Speed, LastSpeed;
            int     FallTime;
            int     Score;  // ����
            int     Lines;  // ���-�� �����
            bool    bPause;
            bool    bGameOver;
            bool    IsDrawGrid;

            TmyShape *pCurrShape, *pNextShape;
            TImage  *pMainImg, *pNextShapeImg;

        	void	ClearField(); // clear Field
            TmyShape *MakeNewShape(TPoint _pos);
            void    GetNewShape();
            bool    CheckShape();
            void    CatchShape();

            void    ClrScreen(TColor, TImage *pImg);
            void    DrawShape(TmyShape *pShape, TPoint _pos, TImage *pImg);
            void    DrawGrid(TColor color, TImage *pImg);
            void    DrawGlass(TImage *pImg);
            void    Draw();

            bool    TestHit();
            void    DelLines();
            void    LevelUp();

            void    GameOver();

    public:
            void    Pause();
            bool    IsGameOver(){return bGameOver;};
            int     GetLevel(){return Level;};
            int     GetScore(){return Score;};
            void    BoolGrid(bool _IsDrawGrid){IsDrawGrid = _IsDrawGrid;};

            TTetris(int rows = 40, int cols = 15,
                    TImage *_MainImg        = 0,
                    TImage *_NextShapeImg   = 0);
            ~TTetris();

            void NewGame();//int _level);
            void Game();
            void KeyPressProc(WPARAM _key, int _state);
};


//---------------------------------------------------------------------------
#endif
