object F_Scores: TF_Scores
  Left = 413
  Top = 153
  BorderStyle = bsToolWindow
  Caption = ' Best Scores'
  ClientHeight = 364
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sPanel1: TsPanel
    Left = 11
    Top = 10
    Width = 266
    Height = 344
    Caption = 'sPanel1'
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object Score_LV: TListView
      Left = 1
      Top = 1
      Width = 264
      Height = 342
      Align = alClient
      Columns = <
        item
          Caption = '#'
          MaxWidth = 30
          MinWidth = 30
          Width = 30
        end
        item
          Caption = 'Score'
          MaxWidth = 80
          MinWidth = 80
          Width = 80
        end
        item
          Caption = 'Name'
          MaxWidth = 150
          MinWidth = 150
          Width = 150
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      RowSelect = True
      ParentFont = False
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
  object sSkinProvider1: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 96
    Top = 48
  end
end
