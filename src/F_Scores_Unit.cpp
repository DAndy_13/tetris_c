//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "TScoreList.h"
#include "F_Scores_Unit.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "sSkinProvider"
#pragma link "sPanel"
#pragma resource "*.dfm"
TF_Scores *F_Scores;

extern TScoreList  *ScoreList;

//---------------------------------------------------------------------------
__fastcall TF_Scores::TF_Scores(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TF_Scores::FormCreate(TObject *Sender)
{
    F_Scores->Position    = poDesktopCenter;

}
//---------------------------------------------------------------------------




void __fastcall TF_Scores::FormShow(TObject *Sender)
{
    myList_type myListItem;// = 0; //ScoreList->GetItem(0);
    TListView *pLV = Score_LV;
    TListItem *ListItem;

    pLV->Items->Clear();


    for(int i = 0; i < ScoreList->GetCount(); i++)
    {
        ScoreList->GetItem(i, &myListItem);

        ListItem = pLV->Items->Add();
        ListItem->Caption = IntToStr(i);
        ListItem->SubItems->Add(IntToStr(myListItem.Score));
        ListItem->SubItems->Add(myListItem.Name);

    }


}
//---------------------------------------------------------------------------
