//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include "sSkinManager.hpp"
#include "sSkinProvider.hpp"
//---------------------------------------------------------------------------
class TF_Main : public TForm
{
__published:	// IDE-managed Components
    TImage *MainImg;
    TLabel *ScoreLbl;
    TImage *NextShapeImg;
    TTimer *GameTimer;
    TLabel *LevelLbl;
    TMainMenu *MainMenu;
    TMenuItem *mNewGame;
    TMenuItem *mPause;
    TMenuItem *mBestScores;
    TButton *Button1;
    TMenuItem *About1;
    TButton *Button2;
    TEdit *Edit1;
    TEdit *Edit2;
    TMenuItem *mPlayer;
    TMenuItem *Settings1;
    TMenuItem *Grid1;
    TMenuItem *mGridOn;
    TMenuItem *mGridOff;
    TsSkinManager *sSkinManager1;
    TsSkinProvider *sSkinProvider1;
    void __fastcall GameTimerTimer(TObject *Sender);
    void __fastcall mNewGameClick(TObject *Sender);
    void __fastcall mPauseClick(TObject *Sender);
    void __fastcall mBestScoresClick(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall About1Click(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall mPlayerClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall mGridOnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TF_Main(TComponent* Owner);



//void __fastcall KeyboardProc(TMessage &Message);
void __fastcall AppMessageHandler(MSG &Msg, bool &Handled);

//BEGIN_MESSAGE_MAP
//    MESSAGE_HANDLER ( WM_KEYUP,  TMessage , KeyboardProc )

    //� �.�. ��� ������ ��������� ��� ���� �������
//END_MESSAGE_MAP(TForm)//TComponent)

};



//---------------------------------------------------------------------------
extern PACKAGE TF_Main *F_Main;
//---------------------------------------------------------------------------
#endif
