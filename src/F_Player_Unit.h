//---------------------------------------------------------------------------

#ifndef F_Player_UnitH
#define F_Player_UnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "sSkinProvider.hpp"
#include "sBitBtn.hpp"
#include "sEdit.hpp"
#include "sPanel.hpp"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TF_Player : public TForm
{
__published:	// IDE-managed Components
    TsSkinProvider *sSkinProvider1;
    TsPanel *sPanel1;
    TsEdit *PlayerNameEdit;
    TsBitBtn *sBitBtn1;
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall sBitBtn1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TF_Player(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TF_Player *F_Player;
//---------------------------------------------------------------------------
#endif
