//---------------------------------------------------------------------------


#pragma hdrstop


#include "TScoreList.h"
#include <stdio.h>



//TScoreList::TScoreList(AnsiString  _FileName)
TScoreList::TScoreList(int _Length, AnsiString  _FileName)
{
/*
    if(_FileName != "")
        FileName = _FileName;
    else
        FileName = "FileName.dat";
*/
    if(_Length <= 0 || _FileName == "")
        return;

    Length = _Length;
    FileName = _FileName;
    
    fs = new TMemoryStream();

    myList = new myList_type[Length+1];

    for(int indx = 0; indx < Length; indx++){
        myList[indx].Score  = 0;
        snprintf(&myList[indx].Name[0], 8, "Player");
    }

    if(FileExists(FileName)){
        fs->LoadFromFile(FileName);
        fs->Position = 0;
        for(int indx = 0; indx < fs->Size/sizeof(myList_type); indx++){
            fs->Read((void *)&myList[indx], sizeof(myList_type));
        }
    }

}
//-------------------------------------------


TScoreList::~TScoreList()
{
    if(fs)
        delete fs;
    if(myList)
        delete []myList;        
}
//-------------------------------------------



//void TScoreList::AddNewScore(int _Score, AnsiString  _Name)
void TScoreList::Add(int _Score, AnsiString  _Name)
{
    if(!fs)
        return;

    myList_type myListItem = {0};
    myListItem.Score = _Score;
    memset(&myListItem.Name[0], 0, sizeof(myListItem.Name));
    snprintf(&myListItem.Name[0], sizeof(myListItem.Name)-1, "%s", _Name);

    int indx = 0;
    for(indx = 0; indx < Length; indx++){
        if(myList[indx].Score < myListItem.Score)
            break;
    }
    // ������� ���
    for(int i = Length; i >indx ; i--){
        myList[i] = myList[i-1];
    }
    // ��������� �����
    myList[indx] = myListItem;

    // ���������
    fs->Seek(0, soFromBeginning);
    for(int i = indx; i < Length; i++){
        fs->Write((void *)&myList[i], sizeof(myList_type));
    }
    fs->SaveToFile(FileName);
}
//-------------------------------------------




void TScoreList::GetItem(int indx, myList_type *myListItem)
{
    if(indx >= 0 && indx < Length){
        myListItem->Score   = myList[indx].Score;
        memcpy((void *)myListItem->Name, myList[indx].Name, sizeof(myList[indx].Name));
    }
}

/*
myList_type *TScoreList::GetItem(int indx)
{
    if(indx < 0 || indx > Length){
        return &myList[indx];
    }
}
*/
//-------------------------------------------





//---------------------------------------------------------------------------

#pragma package(smart_init)




