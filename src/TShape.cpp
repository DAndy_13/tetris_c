//---------------------------------------------------------------------------


#pragma hdrstop

#include "TShape.h"





TmyShape::TmyShape(TColor  _color)
{
    color = _color;
    pos.x = 0;
    pos.y = 0;
    size = 4;
    // ���������� ���������� ������������� ������� �� _size_ ���������:
    cells = new int* [size]; // _size_ ����� � �������
    for (int count = 0; count < size; count++){
        cells[count] = new int [size]; // � �������
    }
    //  ��� cells  � ������ ���������� �� ���������� ������� ������ ��� ������ �����

    Clear();
}
//---------------------------------------------------


TmyShape::~TmyShape()
{
    // ������������� ������ ��������� ��� ��������� ������������ ������:
    for (int count = 0; count < size; count++){
        delete [] cells[count];
    }
    //      ��� _size_ � ���������� ����� � �������
}
//---------------------------------------------------



void TmyShape::Clear()
{
	for( int row = 0; row < size; row++){
		for( int col = 0; col < size; col++){
			cells[row][col] = 0;//row;//0;
        }
    }
}
//---------------------------------------------------



void TmyShape::Rotate()
{
    // ���������� ���������� ������������� ������� �� _size_ ���������:
    int **tmpcells = new int* [size]; // _size_ ����� � �������
    for (int count = 0; count < size; count++){
        tmpcells[count] = new int [size]; // � �������
    }

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            tmpcells[row][col] = cells[row][col];
        }
    }

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            cells[row][col] = tmpcells[col][(size-row)-1];
        }
    }

    // ������������� ������ ��������� ��� ��������� ������������ ������:
    for (int count = 0; count < size; count++){
        delete [] tmpcells[count];
    }
}
//---------------------------------------------------




void TmyShape::UnRotate()
{
    // ���������� ���������� ������������� ������� �� _size_ ���������:
    int **tmpcells = new int* [size]; // _size_ ����� � �������
    for (int count = 0; count < size; count++){
        tmpcells[count] = new int [size]; // � �������
    }

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            tmpcells[row][col] = cells[row][col];
        }
    }

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            //cells[row][col] = tmpcells[col][(size-row)-1];
            cells[col][row] = tmpcells[(size-row)-1][col];
        }
    }

    // ������������� ������ ��������� ��� ��������� ������������ ������:
    for (int count = 0; count < size; count++){
        delete [] tmpcells[count];
    }

}
//---------------------------------------------------





void TmyShape::Move(ShapeDir_type dir)
{
    int dx = 0, dy = 0;
    switch (dir){
        case Left:
            dx = -1;    dy = 0;     break;

        case Right:
            dx = 1;     dy = 0;     break;

        case Down:
            dx = 0;     dy = 1;     break;

        case Up:
            dx = 0;     dy = -1;    break;
    }

    pos.x += dx;
    pos.y += dy;
}
//---------------------------------------------------





//---------------------------------------------------
//---------------------------------------------------
//---------------------------------------------------
TmyLine::TmyLine(TColor  _color) : TmyShape(_color)
{
    for(int row =0; row < size; row++){
       cells[row][1] = 1;
    }

}


TmyRightL::TmyRightL(TColor  _color) : TmyShape(_color)
{
    cells[0][1] = 2;
    cells[1][1] = 2;
    cells[2][1] = 2;
    cells[2][2] = 2;
}

TmyLeftL::TmyLeftL(TColor  _color) : TmyShape(_color)
{
    cells[0][1] = 3;
    cells[1][1] = 3;
    cells[2][1] = 3;
    cells[0][2] = 3;
}


TmyLeftZ::TmyLeftZ(TColor  _color) : TmyShape(_color)
{
    cells[0][1] = 4;
    cells[1][1] = 4;
    cells[1][2] = 4;
    cells[2][2] = 4;
}


TmyRightZ::TmyRightZ(TColor  _color) : TmyShape(_color)
{
    cells[0][2] = 5;
    cells[1][2] = 5;
    cells[1][1] = 5;
    cells[2][1] = 5;
}


TmyPiramide::TmyPiramide(TColor  _color) : TmyShape(_color)
{
    cells[0][1] = 6;
    cells[1][1] = 6;
    cells[2][1] = 6;
    cells[1][2] = 6;
}

TmySqare::TmySqare(TColor  _color) : TmyShape(_color)
{
    cells[1][1] = 7;
    cells[1][2] = 7;
    cells[2][1] = 7;
    cells[2][2] = 7;
}


//---------------------------------------------------------------------------
#pragma package(smart_init)
