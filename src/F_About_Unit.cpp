//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "F_About_Unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "sSkinProvider"
#pragma link "sButton"
#pragma link "sPanel"
#pragma link "sRichEdit"
#pragma resource "*.dfm"
TF_About *F_About;
//---------------------------------------------------------------------------
__fastcall TF_About::TF_About(TComponent* Owner)
    : TForm(Owner)
{
    AnsiString text = "";

    F_About->Position = poDesktopCenter;

    AboutRichE->Lines->Clear();
    AboutRichE->SelAttributes->Name = "Arial";
    AboutRichE->SelAttributes->Size = 10;
    text =  " Tetris";
    text +=  RELEASE_ID;
    text +=  "\r\n\r\n";
    text += " ����������:\r\n\r\n";
//    AboutRichE->Lines->Add(text);
    text += " Esc - �����\r\n";
    text += " Up - �������� ������\r\n";
    text += " Down - ������� ������\r\n";
    text += " Space - ������� ������\r\n";
    text += "\r\n";
    text += " (�)������� ������\r\n";
    text += " �������. 2012�.\r\n\r\n";
    text += " Andy-pix@mail.ru\r\n";
    AboutRichE->Lines->Add(text);

    AboutRichE->SelAttributes->Color = clBlue;
    AboutRichE->SelAttributes->Size = 16;
    text = " http://andy-pix.livejournal.com\r\n";
    AboutRichE->Lines->Add(text);

    
}
//---------------------------------------------------------------------------
void __fastcall TF_About::BitBtn1Click(TObject *Sender)
{
    this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TF_About::OKClick(TObject *Sender)
{
    this->Close();
}
//---------------------------------------------------------------------------

