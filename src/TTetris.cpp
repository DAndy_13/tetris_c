//---------------------------------------------------------------------------


#pragma hdrstop

#include "TTetris.h"



// ���������e �������
TColor GetDarker(TColor Color, int Percent)
{
    int r,g,b;
    if(Percent > 100)
        Percent = 100;

    Color = ColorToRGB(Color);

    r = GetRValue(Color);
    g = GetGValue(Color);
    b = GetBValue(Color);
    r -= (r * Percent)/100;
    g -= (g * Percent)/100;
    b -= (b * Percent)/100;
    if(r < 0) r = 0;
    if(g < 0) g = 0;
    if(b < 0) b = 0;

    return (TColor)RGB(r,g,b);
}


// ���������e �������
TColor GetLighter(TColor Color, int Percent)
{
    int r,g,b;
    if(Percent > 100)
        Percent = 100;

    Color = ColorToRGB(Color);

    r = GetRValue(Color);
    g = GetGValue(Color);
    b = GetBValue(Color);
    r += (r * Percent)/100;
    g += (g * Percent)/100;
    b += (b * Percent)/100;
    if(r > 255) r = 255;
    if(g > 255) g = 255;
    if(b > 255) b = 255;

    return (TColor)RGB(r,g,b);
}






static TColor mColor[] = {clFuchsia, clRed, clGreen, clBlue, clAqua, clPurple, clLime};

enum ShapeType {
    line = 0,
    square,
    rightL,
    leftL,
    pyramide,
    leftZ,
    rightZ
};


TTetris::TTetris(int rows, int cols,
                    TImage *_MainImg,
                    TImage *_NextShapeImg)
{
    if(!_MainImg || !_NextShapeImg || rows < 10 || cols < 5)
        return;

    pMainImg        = _MainImg;
    pNextShapeImg   = _NextShapeImg;

    RowCount    = rows;
    ColCount    = cols;
    Step        = _STEP;

    Score       = 0;
    Level       = 1;
    Speed       = MIN_SPEED - Level;
    FallTime    = Speed;
    LastSpeed   = Speed;

    bPause      = false;
    bGameOver   = true;
    IsDrawGrid  = false;


    // ���������� ���������� ������������� ������� �� _size_ ���������:
    Field = new TColor* [ColCount]; // _size_ ����� � �������
    for (int count = 0; count < ColCount; count++){
        Field[count] = new TColor [RowCount]; // � �������
    }
    //  ��� Field  � ������ ���������� �� ���������� ������� ������ ��� ������ �����
    ClearField();

    StartPos.x = 0;
    StartPos.y = 0;

    pCurrShape  = 0;     pNextShape  = 0;
    GetNewShape();       GetNewShape();

    // ��������� ��������� ����� ������
    StartPos.x = (ColCount - pCurrShape->GetSize())/2;
    StartPos.y = 0;
    GetNewShape();       GetNewShape();

    // ������� � ��������� ����
    pMainImg->Top           = Step;
    pMainImg->Left          = Step;
    pMainImg->Width         = (ColCount * Step) +1;
    pMainImg->Height        = (RowCount * Step) +1;
    pNextShapeImg->Width    = ((pCurrShape->GetSize()+2) * Step) +1;
    pNextShapeImg->Height   = ((pCurrShape->GetSize()+2) * Step) +1;
    pNextShapeImg->Left     = pMainImg->Left + pMainImg->Width + Step;
    pNextShapeImg->Top      = pMainImg->Top;

    ClrScreen(_BackColor/*clGray*/, pMainImg);
}
//---------------------------------------------------




TTetris::~TTetris()
{
    // ������������� ������ ��������� ��� ��������� ������������ ������:
    for (int count = 0; count < RowCount; count++){
        delete [] Field[count];
    }
}
//---------------------------------------------------



void TTetris::ClearField()
{
	for( int row = 0; row < ColCount; row++){
		for( int col = 0; col < RowCount; col++){
			Field[row][col] = TColor(0);
        }
    }
}
//---------------------------------------------------



TmyShape *TTetris::MakeNewShape(TPoint _pos)
{
    randomize();
    TmyShape *pShape = 0;
    TColor color = mColor[random(7)];
//    int indx = random(7);
    int indx = random(65535)%7;
    switch(indx)
    {
        case line:      pShape = new TmyLine(color);      break;
        case square:    pShape = new TmySqare(color);     break;
        case rightL:    pShape = new TmyRightL(color);    break;
        case leftL:     pShape = new TmyLeftL(color);     break;
        case pyramide:  pShape = new TmyPiramide(color);  break;
        case leftZ:     pShape = new TmyLeftZ(color);     break;
        case rightZ:    pShape = new TmyRightZ(color);    break;
    }

    pShape->SetPos(_pos);
    return pShape;
}

//---------------------------------------------------



void TTetris::GetNewShape()
{
    if(pCurrShape)
        delete pCurrShape;

    Speed       = MIN_SPEED - Level;
    LastSpeed   = Speed;

    pCurrShape  = pNextShape;
    pNextShape  = MakeNewShape(StartPos);

    if(pCurrShape){
        if(TestHit())
            GameOver();
    }

//            clBlack    
    ClrScreen(_BackColor/*clGray*/, pNextShapeImg);
    TPoint pos;
    pos.x = 1;
    pos.y = 1;
    if(!bGameOver){
        DrawShape(pNextShape, pos, pNextShapeImg);
        DrawGrid(GetDarker(clDkGray, 40), pNextShapeImg);
    }
//    DrawGrid(clDkGray, pNextShapeImg);  

}
//---------------------------------------------------


void TTetris::LevelUp()
{
    if(Level++ > MAX_LEVEL)
        Level = MAX_LEVEL;
    Speed = MIN_SPEED - Level;
    LastSpeed = Speed;
};
//---------------------------------------------------



void TTetris::ClrScreen(TColor color, TImage *pImg)
{
    TCanvas *canva = pImg->Canvas;
    canva->Brush->Style = bsSolid;
    canva->Brush->Color = color;
    canva->FillRect(pImg->ClientRect);
}
//---------------------------------------------------



void TTetris::DrawShape(TmyShape *pShape, TPoint _pos, TImage *pImg)
{
    TCanvas *canva = pImg->Canvas;
    int size    = pShape->GetSize();
    TRect rect;

    canva->Brush->Color = pShape->GetColor();
    canva->Pen->Color   = GetDarker(pShape->GetColor(), 20);
//    canva->Pen->Color   = clDkGray;
    int dt = 0;
    canva->Pen->Width   = dt;

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            if(pShape->cells[row][col]){
                rect.Left   = (_pos.x + col)*Step;
                rect.Top    = (_pos.y + row)*Step;
                rect.right  = rect.Left + Step;
                rect.bottom = rect.Top + Step;
                canva->FillRect(rect);
            }
        }
    }
    canva->Pen->Width   = 1;
}
//---------------------------------------------------
             


void TTetris::DrawGlass(TImage *pImg)
{
    TCanvas *canva = pImg->Canvas;
    TRect rect;
//    TColor  color = clWhite;
    for(int col = 0; col < ColCount; col++){
        for(int row = 0; row < RowCount; row++){
            if(Field[col][row])
            {
                rect.Left   = col * Step;
                rect.Top    = row * Step;
                rect.right  = rect.Left + Step;
                rect.bottom = rect.Top + Step;

                //canva->Brush->Color = color;
                canva->Brush->Color = Field[col][row];
                canva->FillRect(rect);
            }
        }
//        color -= 50;
    }

    canva->Pen->Color = clWhite;
    canva->Pen->Width = 2;
    canva->MoveTo(0, 0);
    canva->LineTo(0, pImg->Height);

    canva->MoveTo(0, pImg->Height);
    canva->LineTo(pImg->Width, pImg->Height);

    canva->MoveTo(pImg->Width, pImg->Height);
    canva->LineTo(pImg->Width, 0);

    /*
    for(int row = 0; row < ColCount; row++){
        for(int col = 0; col < RowCount; col++){
            if(Field[row][col])
            {
//                rect.Left   = col * Step;
//                rect.Top    = row * Step;
                rect.Left   = row * Step;
                rect.Top    = col * Step;
                rect.right  = rect.Left + Step;
                rect.bottom = rect.Top + Step;

//                canva->Brush->Color = color;
                canva->Brush->Color = Field[row][col];
                canva->FillRect(rect);
            }
        }
        //color -= 50;
    }
*/
    
}
//---------------------------------------------------



void TTetris::DrawGrid(TColor color, TImage *pImg)
{
    if(!IsDrawGrid)
        return;

    TCanvas *canva = pImg->Canvas;
    canva->Pen->Color = color;

    int Width   = pImg->Width;
    int Height  = pImg->Height;
    for(int i = 0; i < Width; i += Step ){
        canva->MoveTo(i, 0);
        canva->LineTo(i, Height);
    }
    for(int j = 0; j < Height; j += Step){
        canva->MoveTo(0, j);
        canva->LineTo(Width, j);
    }
}
//---------------------------------------------------




void TTetris::CatchShape()
{
    int size    = pCurrShape->GetSize();
    TPoint pos  = pCurrShape->GetPos();

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            if(pCurrShape->cells[row][col]){
                Field[col + pos.x][row + pos.y] = pCurrShape->GetColor();
            }
        }
    }
}
//---------------------------------------------------




bool TTetris::TestHit()
{
    bool ret    = false;
    int size    = pCurrShape->GetSize();
    TPoint pos  = pCurrShape->GetPos();

    for(int row = size-1; row >=0 ; row--){
        for(int col = 0; col < size; col++){
            if(pCurrShape->cells[row][col]){
                if( Field[col + pos.x][row + pos.y + 1] || ((row + pos.y) == RowCount-1))
                    return ret = true;
            }
        }
    }  
    return ret;
}
//---------------------------------------------------





bool TTetris::CheckShape()
{
    bool ret    = true;
    int size    = pCurrShape->GetSize();
    TPoint pos  = pCurrShape->GetPos();

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            if(pCurrShape->cells[row][col]){
                if( (col + pos.x < 0) || (col + pos.x >= ColCount ) || Field[col + pos.x][row + pos.y])
                    return ret = false;
            }
        }
    }
    return ret;
}
//---------------------------------------------------





void TTetris::DelLines()
{
    int count_block = 0;
/*
    for(int col = 0; col < ColCount; col++){
        for(int row = 0; row < RowCount; row++){
            if(Field[col][row])
        }
    }
*/
	for( int row = RowCount-1; row >0; row--){
        count_block = 0;
		for( int col = 0; col < ColCount; col++){
			if(Field[col][row]){
                count_block++;
            }
        }
        if(count_block == ColCount){
        //    MoveLines();
            for( int _row = row; _row > 0; _row--){
                for( int _col = 0; _col < ColCount; _col++){
                    Field[_col][_row] = Field[_col][_row -1];
                }
            }
            row++;
            Score += Level * 10;
            Lines ++;
            if(Lines >= MAX_LINES){
                LevelUp();
                Lines = 0;
            }
        }
    }

}
//---------------------------------------------------



void TTetris::GameOver()
{
    bGameOver = true;
    
    AnsiString text = "Game over!";

    Graphics::TBitmap  *pbitmap = new Graphics::TBitmap();
    TCanvas  *pCanva = pbitmap->Canvas;//new TCanvas();
    int width   = pCanva->TextWidth(text);
    int height  = pCanva->TextHeight(text);

    pMainImg->Canvas->Brush->Color  = clRed;//clDkGray;
    pMainImg->Canvas->Font->Color   = clBlack;//clRed;
    pMainImg->Canvas->Font->Style   = TFontStyles() << fsBold;
    pMainImg->Canvas->Font->Name    = "Arial";
    pMainImg->Canvas->Font->Size    = 16;

    int x = pMainImg->Width/2 - width;// - TextWidth(text))/2;
    int y = pMainImg->Height/2 - height/2;
    pMainImg->Canvas->TextRect(pMainImg->ClientRect, x, y, text);

    delete pbitmap;

    // ������� ��������

    
}
//---------------------------------------------------




void TTetris::Pause()
{
    if(bGameOver)
        return;

    bPause ^= true; // �����/�����

    AnsiString text = "Pause";

    Graphics::TBitmap  *pbitmap = new Graphics::TBitmap();
    TCanvas  *pCanva = pbitmap->Canvas;//new TCanvas();
    int width   = pCanva->TextWidth(text);
    int height  = pCanva->TextHeight(text);

    pMainImg->Canvas->Brush->Color  = clDkGray;
    pMainImg->Canvas->Font->Color   = clRed;
    pMainImg->Canvas->Font->Style   = TFontStyles() << fsBold;
    pMainImg->Canvas->Font->Name    = "Arial";
    pMainImg->Canvas->Font->Size    = 16;

    int x = pMainImg->Width/2 - width;// - TextWidth(text))/2;
    int y = pMainImg->Height/2 - height/2;
    pMainImg->Canvas->TextRect(pMainImg->ClientRect, x, y, text);

    delete pbitmap;
}
//---------------------------------------------------






#define PRESS 0
#define UNPRESS 1
void TTetris::KeyPressProc(WPARAM _key, int _state)
{
    switch(_key)
    {
        case VK_LEFT:
            if(pCurrShape && _state == PRESS){
                pCurrShape->Move(Left);
                if(!CheckShape())
                    pCurrShape->Move(Right);
            }
            break;

        case VK_RIGHT:
            if(pCurrShape && _state == PRESS){
                pCurrShape->Move(Right);
                if(!CheckShape())
                    pCurrShape->Move(Left);
            }
            break;

        case VK_DOWN:
            if(_state == PRESS){
                FallTime    = FALL_SPEED;
            }
            if(_state == UNPRESS){
                FallTime    = LastSpeed;
            }
            break;

        case VK_UP:
            if(pCurrShape && _state == PRESS){
                pCurrShape->Rotate();
                if(!CheckShape())
                    pCurrShape->UnRotate();
            }
            break;

        case VK_SPACE:
            if(_state == PRESS){
                FallTime    = FALL_SPEED;
                Speed       = FALL_SPEED;
            }
            break;

        case VK_ESCAPE:
            if(_state == PRESS){
                Pause();
            }
            break;

         default:
            break;
    }
}
//---------------------------------------------------





void TTetris::NewGame()//int _level)
{
    Score       = 0;
    Level       = 1;//_level;
    Speed       = MIN_SPEED - Level;
    FallTime    = Speed;
    LastSpeed   = Speed;

    bPause      = false;
    bGameOver   = false;

    ClrScreen(_BackColor, pMainImg);
    ClearField();
    GetNewShape();           
}
//---------------------------------------------------




void TTetris::Game()
{
    if(bPause || bGameOver)
        return;
        
    ClrScreen(_BackColor, pMainImg);
    DrawGlass(pMainImg);        // ��������� �������

    if(pCurrShape){
        if(FallTime-- == 0){
//            pCurrShape->Move(Down);
            FallTime = Speed;//LastSpeed;

            if(TestHit()){
                CatchShape();   // ������� ������ � ������
                DrawShape(pCurrShape, pCurrShape->GetPos(), pMainImg);
                GetNewShape();  // ������� ����� ������

                DelLines();     // ��������� � ������� ������ �����
            }
            pCurrShape->Move(Down);
        }
        if(!bGameOver)
            DrawShape(pCurrShape, pCurrShape->GetPos(), pMainImg);
    }

    if(!bGameOver){
        DrawGrid(GetDarker(clDkGray, 40), pMainImg);
    }
//    DrawGrid(clBlack, pMainImg);

}
//---------------------------------------------------





//---------------------------------------------------------------------------

#pragma package(smart_init)
