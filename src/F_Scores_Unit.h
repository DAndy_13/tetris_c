//---------------------------------------------------------------------------

#ifndef F_Scores_UnitH
#define F_Scores_UnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "sSkinProvider.hpp"
#include "sPanel.hpp"
//---------------------------------------------------------------------------
class TF_Scores : public TForm
{
__published:	// IDE-managed Components
    TsSkinProvider *sSkinProvider1;
    TsPanel *sPanel1;
    TListView *Score_LV;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TF_Scores(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TF_Scores *F_Scores;
//---------------------------------------------------------------------------
#endif
