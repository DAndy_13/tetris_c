//---------------------------------------------------------------------------

#ifndef F_About_UnitH
#define F_About_UnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "sSkinProvider.hpp"
#include "sButton.hpp"
#include "sPanel.hpp"
#include "sRichEdit.hpp"
//---------------------------------------------------------------------------
class TF_About : public TForm
{
__published:	// IDE-managed Components
    TsSkinProvider *sSkinProvider1;
    TsPanel *sPanel1;
    TsButton *OK;
    TsRichEdit *AboutRichE;
    void __fastcall BitBtn1Click(TObject *Sender);
    void __fastcall OKClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TF_About(TComponent* Owner);
};


#define RELEASE_ID " v. 1.0.00"

//---------------------------------------------------------------------------
extern PACKAGE TF_About *F_About;
//---------------------------------------------------------------------------
#endif
