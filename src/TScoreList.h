//---------------------------------------------------------------------------

#ifndef TScoreListH
#define TScoreListH

#include <vcl.h>

typedef struct{
    int         Score;
    char        Name[16];
}myList_type;



class TScoreList
{
    private: 
            myList_type     *myList;
            TMemoryStream   *fs;
            AnsiString      FileName;
            int             Length;

            //void Sort();

    public:
            void        Add(int _Score, AnsiString  _Name);
            void        SetFileName(AnsiString  _FileName){FileName = _FileName;};
            int         GetCount(){return Length;};
            //myList_type *GetItem(int indx);
            void        GetItem(int indx, myList_type *myListItem);

            TScoreList(int _Length, AnsiString  _FileName);
            ~TScoreList();
};

//---------------------------------------------------------------------------
#endif
