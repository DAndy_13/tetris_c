//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("main.cpp", F_Main);
USEFORM("F_About_Unit.cpp", F_About);
USEFORM("F_Scores_Unit.cpp", F_Scores);
USEFORM("F_Player_Unit.cpp", F_Player);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->Title = "Andy's Tetris";
         Application->CreateForm(__classid(TF_Main), &F_Main);
         Application->CreateForm(__classid(TF_About), &F_About);
         Application->CreateForm(__classid(TF_Scores), &F_Scores);
         Application->CreateForm(__classid(TF_Player), &F_Player);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
